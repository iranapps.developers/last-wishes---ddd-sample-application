<?php

namespace Lw\Application\Service\Wish;

class AddWishRequest
{
    private $userId;
    private $email;
    private $content;
    private $address;

	/**
	 * @param string $userId
	 * @param string $email
	 * @param string $content
	 * @param null $address
	 */
    public function __construct($userId, $email, $content, $address = null)
    {
        $this->userId = $userId;
        $this->email = $email;
        $this->content = $content;
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function userId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function email()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

	/**
	 * @return mixed
	 */
	public function address()
	{
		return $this->address;
	}
}
